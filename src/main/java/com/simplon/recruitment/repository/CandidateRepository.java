package com.simplon.recruitment.repository;

import com.simplon.recruitment.entity.Candidat;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CandidateRepository extends JpaRepository<Candidat, Integer> {

}
