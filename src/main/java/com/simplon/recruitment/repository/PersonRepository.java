package com.simplon.recruitment.repository;

import com.simplon.recruitment.entity.Person;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(path = "person")
public interface PersonRepository extends JpaRepository<Person, Integer>  {
    
}
