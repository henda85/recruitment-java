package com.simplon.recruitment.repository;

import com.simplon.recruitment.entity.Recruiter;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(path = "recruiter")
public interface RecruiterRepeository extends JpaRepository<Recruiter, Integer> {
    
}
