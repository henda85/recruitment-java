package com.simplon.recruitment.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@DiscriminatorValue("R")
@Table(name = "Recruiter")
public class Recruiter extends Person {

    private String domain;

    public Recruiter() {
    }

    public Recruiter(Integer id, String name, String address, String phone, String email) {
        super(id, name, address, phone, email);
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

}
