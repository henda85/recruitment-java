package com.simplon.recruitment.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.GenerationType;

@Entity
public class JobAdvert {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    private LocalDate date;

    @ManyToOne
    @JoinColumn(name = "idRecuiter")
    private Recruiter recruiter;

    @ManyToMany
    @JoinTable(name = "JobAdvert_candidat", joinColumns = @JoinColumn(name = "idJobAdvert"), inverseJoinColumns = @JoinColumn(name = "idCandidat"))
    private List<Candidat> candidats = new ArrayList<>();

    public JobAdvert() {
    }

    public JobAdvert(Integer id, String title, LocalDate date) {
        this.id = id;
        this.title = title;
        this.date = date;
    }

    public JobAdvert(String title, LocalDate date) {
        this.title = title;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

}
