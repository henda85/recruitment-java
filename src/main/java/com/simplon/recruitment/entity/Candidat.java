package com.simplon.recruitment.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@DiscriminatorValue("C")
@Table(name = "Candidat")
public class Candidat extends Person {

    public Candidat(Integer id, String name, String address, String phone, String email) {
        super(id, name, address, phone, email);
    }

    public String cvLink;
    public String portfolioLink;

    @ManyToMany
    @JoinTable(name = "JobAdvert_candidat", joinColumns = @JoinColumn(name = "idCandidat"), inverseJoinColumns = @JoinColumn(name = "idJobAdvert"))
    private List<JobAdvert> jobAdverts = new ArrayList<>();

  

    public Candidat() {
    }

    public String getCvLink() {
        return cvLink;
    }

    public void setCvLink(String cvLink) {
        this.cvLink = cvLink;
    }

    public String getPortfolioLink() {
        return portfolioLink;
    }

    public void setPortfolioLink(String portfolioLink) {
        this.portfolioLink = portfolioLink;
    }

}
