package com.simplon.recruitment.ui;

import javax.swing.JFrame;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class mainWindow extends JFrame {

    // @Autowired
    // private ListCandidat listCandidat;
    @Autowired
    private ListJobAdvert listJobAdvert;

    public mainWindow() {
        setTitle("JobAdvert");
        // getContentPane().setLayout(new FlowLayout());// specify a layout manager
        // getContentPane().setBackground(Color.red);
        // setVisible(true);
        setSize(500, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void run() {

        setContentPane(listJobAdvert);
        setVisible(true);
    }

}
