package com.simplon.recruitment.ui;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.simplon.recruitment.Controller.CandidatController;
import com.simplon.recruitment.entity.Candidat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ListCandidat extends JPanel {

    @Autowired
    private CandidatController conroller;

    public ListCandidat() {

        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
    }

    @PostConstruct

    public void init() {
        List<Candidat> liste = conroller.allCandidat();

        for (Candidat candidat : liste) {
            add(new JLabel(
                    "<html>NameCandidat : " + candidat.getName() + " EmailCandidat : " + candidat.getEmail()
                            + "</html>"));

        }
    }

}
