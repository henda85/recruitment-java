package com.simplon.recruitment.ui;

import java.awt.Color;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.simplon.recruitment.Controller.JobAdvertController;
import com.simplon.recruitment.entity.JobAdvert;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ListJobAdvert extends JPanel {

    @Autowired
    private JobAdvertController conroller;

    public ListJobAdvert() {

        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
    }

    @PostConstruct

    public void init() {
        List<JobAdvert> liste = conroller.allJobAdvert();

        for (JobAdvert jobAdvert : liste) {
            add(new JLabel(
                    "<html><font color='blue'>Title : </font>" + jobAdvert.getTitle()
                            + "<font color='blue'> Date :</font> " +
                            jobAdvert.getDate()
                            + "</html>"));
            setBorder(BorderFactory.createLineBorder(Color.gray));
        }
    }

}
