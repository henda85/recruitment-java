package com.simplon.recruitment;

import java.awt.EventQueue;

import com.simplon.recruitment.ui.mainWindow;

import org.springframework.context.ApplicationContext;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class RecruitmentApplication {

	public static void main(String[] args) {

		ApplicationContext ctx = new SpringApplicationBuilder(RecruitmentApplication.class)
				.headless(false).run(args);

		EventQueue.invokeLater(() -> {

			mainWindow window = ctx.getBean(mainWindow.class);
			window.run();
		});

	}

}
