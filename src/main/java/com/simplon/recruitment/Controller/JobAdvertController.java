package com.simplon.recruitment.Controller;

import java.util.List;

import com.simplon.recruitment.entity.JobAdvert;
import com.simplon.recruitment.repository.JobAdvertReository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class JobAdvertController {
    @Autowired
    private JobAdvertReository repo;

    public List<JobAdvert> allJobAdvert() {
        return repo.findAll();
    }

}
