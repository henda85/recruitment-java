package com.simplon.recruitment.Controller;

import java.util.List;
import java.util.Optional;

import com.simplon.recruitment.entity.Candidat;
import com.simplon.recruitment.repository.CandidateRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.server.ResponseStatusException;

@Controller
public class CandidatController {

    @Autowired
    private CandidateRepository repo;

    public List<Candidat> allCandidat() {
        return repo.findAll();
    }

    public Optional<Candidat> oneCandidat(int id) {
        Optional<Candidat> candidat = repo.findById(id);
        if (candidat == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return candidat;
    }
}
