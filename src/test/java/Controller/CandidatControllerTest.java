package Controller;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;

import java.util.List;

import com.simplon.recruitment.Controller.CandidatController;
import com.simplon.recruitment.entity.Candidat;
import com.simplon.recruitment.repository.CandidateRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CandidatControllerTest {
    @InjectMocks
    CandidatController controller;

    @Mock
    CandidateRepository repo;

    @Test
    void testAllCandidat() {
        Candidat candidat = new Candidat();

        Mockito.when(repo.findAll()).thenReturn(List.of(candidat));

        assertTrue(controller.allCandidat().contains(candidat));

        verify(repo).findAll();
    }

}
